@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Your Surveys</div>

                <div class="panel-body">
                  {{ Form::open(array('action' => 'SurveyController@create', 'method' => 'get')) }}

                          {!! Form::submit('Add Survey', ['class' => 'button']) !!}
                          {!! Form::submit('View Responses', ['class' => 'button']) !!}

                  {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
