@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Create a Survey</div>

                <div class="panel-body">
                  {!! Form::open(array('action' => 'SurveyController@store', 'id' => 'createsurvey')) !!}
                  {{ csrf_field() }}

                  {!! Form::label('title', 'Title:') !!}
                  {!! Form::text('title', null, ['class' => 'large-8 columns']) !!}
                  <br>
                  {!! Form::label('title', 'Ethics:') !!}
                  <br>
                  {!! Form::textarea('title', null, ['class' => 'large-8 columns']) !!}
                  <br>
                  {!! Form::label('title', 'Question 1:') !!}
                  {!! Form::text('title', null, ['class' => 'large-8 columns']) !!}
                  <br>
                  {!! Form::label('title', 'Question 2:') !!}
                  {!! Form::text('title', null, ['class' => 'large-8 columns']) !!}
                  <br>
                  {!! Form::label('title', 'Question 3:') !!}
                  {!! Form::text('title', null, ['class' => 'large-8 columns']) !!}
                  <br>
                  {!! Form::label('title', 'Question 4:') !!}
                  {!! Form::text('title', null, ['class' => 'large-8 columns']) !!}
                  <br>
                  {!! Form::label('title', 'Question 5:') !!}
                  {!! Form::text('title', null, ['class' => 'large-8 columns']) !!}
                  <br>
                  {!! Form::submit('Create Survey', ['class' => 'button']) !!}

                  {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
